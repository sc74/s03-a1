INSERT INTO users(username, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", '2021-01-01 01:00'),
("jdc@email.com", "passwordB", '2021-01-01 02:00:00'),
("jane@email.com", "passwordC", '2021-01-01 03:00:00'),
("mar@email.com", "passwordD", '2021-01-01 04:00:00'),
("john@email.com", "passwordE", '2021-01-01 05:00:00');

INSERT INTO posts(title, content, datetime_posted, author_id)
VALUES ("First Code", "Hello World", '2021-01-02 01:00', 1),
("Second Code", "Hello Earth", '2021-01-02 02:00:00', 1),
("Third Code", "Welcome", '2021-01-02 03:00:00', 2),
("Fourth Code", "Byebye", '2021-01-02 04:00:00', 4);

SELECT * FROM posts 
WHERE author_id = 1;

SELECT username, datetime_created 
FROM users;

UPDATE posts
SET content = "Hello to everyone!"
WHERE id = 2;

DELETE FROM users
WHERE username = 'john@email.com';